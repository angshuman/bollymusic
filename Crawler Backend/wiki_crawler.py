import requests
import re
import pymysql
import argparse
from datetime import date

url="http://en.wikipedia.org/wiki/List_of_<filmloc>_films_of_<year>"
filmlocs=['Bollywood','Bengali','Tamil']
cleaner = re.compile("<.*?>")
url_extractor= re.compile("<a href=\"(.*?)\"")
current_year=date.today().year
print current_year
def get_page(url):
	"""
	takes an URL and returns the html text
	"""
	reply=requests.get(url)
	return reply.text

def get_movie_data(text_data):
	"""
	using regex to extract titles including wikipedia links
	"""
	pattern=re.compile("<td><i>(.*?)</i></td>")
	return pattern.findall(text_data)

def sanitise(movie_list):
	"""
	When we get the data from wikipedia and extract data using get_movie_data,
	the list returned contains the title and a link to its corresponding wiki
	page, if it exists. In such a case, we create a new in a dictionary with 
	the name pointing to the URL.
	In some cases, the URL exists, but the page does not exist. Again, it might
	so happen that there's a movie but no URL at all. In such a case, we make 
	the URL field = NULL
	"""
	movies={}
	for movie in movie_list:
		m_name=cleaner.sub("",movie)
		m_url=url_extractor.findall(movie)
		if len(m_url)==1:
			if movie.find("(page does not exist)")>-1:
				movies[m_name]="NULL"
			else:
				movies[m_name]=m_url[0]
		else:
			movies[m_name]="NULL" 	
	return movies

def get_data(filmloc,year):
	page_url=url.replace("<filmloc>",filmloc).replace("<year>",year)
	page=get_page(page_url)
	movie_list=get_movie_data(page)
	movies=sanitise(movie_list)
	return movies
	
def init_db(dbname, dbuser, dbpass, dbhost, dbport=3306):
	"""
	create an instance of a mysql connector. 
	"""
	try:
		connection = pymysql.connect(host=dbhost,user=dbuser,password=dbpass,db=dbname,port=dbport,autocommit=True)
		return connection
	except:
		raise Exception()
def init_parser():
	"""
	create a new instance of the argument parser and defines all parameters
	and subcommands
	"""
	parser=argparse.ArgumentParser()
	#DEFINE GLOBAL ARGUMENTS
	parser.add_argument("--dbuser", required=True, help="Database User", type=str)
	parser.add_argument("--dbpass", required=True, help="Database Password", type=str)
	parser.add_argument("--dbhost", required=True, help="Database Host", type=str)
	parser.add_argument("--dbname", required=True, help="Database Name", type=str)
	parser.add_argument("--dbport", default =3306, help="Database Port", type=int)
	#ADD SUBPARSERS
	subparsers = parser.add_subparsers(help='sub-command help',dest='command')
	#UPDATE SUBPARSER
	update=subparsers.add_parser("update", help="updates the database for current year")
	update.add_argument("--lang",choices=filmlocs, type=str, default=filmlocs[0],help="Language of films to crawl")
	#ALL SUBPARSER
	crawl_all=subparsers.add_parser("all", help="crawl all available movie data for given language")
	crawl_all.add_argument("--lang",choices=filmlocs, type=str, default=filmlocs[0],help="Language of films to crawl")
	#ONLY SUBPARSER
	crawl_only=subparsers.add_parser("only",help="crawl only certain years for given language")
	crawl_only.add_argument("--year",type=int,choices=range(1920,current_year+1),nargs="+",help="One or more years within the range to crawl")
	crawl_only.add_argument("--lang",choices=filmlocs, type=str, default=filmlocs[0],help="Language of films to crawl")
	#RANGE SUBPARSER
	crawl_range=subparsers.add_parser("range",help="crawl range for given language")
	crawl_range.add_argument("--start",type=int, required=True, choices=xrange(1920,current_year), help="Starting Year")
	crawl_range.add_argument("--end",type=int,required=True,choices=xrange(1921,current_year+1),help="Ending Year")
	crawl_range.add_argument("--lang",choices=filmlocs, type=str, default=filmlocs[0],help="Language of films to crawl")
	return parser

def update_database(connection, movie, url, year):
	with connection.cursor() as cursor:
		try:
			cursor.execute("INSERT INTO movies VALUES(%s,%s,%s)",(movie.encode("utf-8"),year,url))
		except pymysql.IntegrityError as IE:
			pass
		except Exception as e:
			print e
			print movie, year, url
	
def command_update(args,connection):
	movies=get_data(args.lang,str(current_year))
	for movie in movies:
		update_database(connection,movie,movies[movie],str(current_year))
	return

def command_all(args,connection):
	for year in range(1920,current_year+1):
		print ("YEAR:%d"%(year))
		movies=get_data(args.lang,str(year))
		for movie in movies:
			update_database(connection,movie,movies[movie],str(year))
	return
	
def command_only(args,connection):
	for year in args.year:
		movies=get_data(args.lang,str(year))
		for movie in movies:
			update_database(connection,movie,movies[movie],str(year))
	return

	
def command_range(args,connection):
	for year in range(args.start,args.end):
		movies=get_data(args.lang,str(year))
		for movie in movies:
			update_database(connection,movie,movies[movie],str(year))
	return
	
call_command={"update":command_update, "all":command_all,"only":command_only,"range":command_range}

def main():
	parser=init_parser()
	args=parser.parse_args()
	connection=None
	try:
		connection= init_db(args.dbname, args.dbuser, args.dbpass, args.dbhost, args.dbport)
	except Exception as e:
		print "Could not open database connection. Please check credentials and connection settings"
		exit(0)
	call_command[args.command](args,connection)
	connection.close()
	
	
if __name__=="__main__":
	main()
	
