CREATE TABLE `movies`(
	`name` VARCHAR(255) NOT NULL,
	`year` SMALLINT NOT NULL,
	`url` VARCHAR(500) DEFAULT NULL,
	PRIMARY KEY (`name`,`year`)
) ENGINE=`innodb` character set = utf8 collate = utf8_unicode_ci;
